﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/SpecularShader" {
	Properties {
		_Color ("Color Tint", Color) = (1,1,1,1)
		_SpecColour ("Specular Color", Color) = (1,1,1,1)
		_Shininess ("Shininess", float) = 10
	}
	SubShader {
			Pass {
					CGPROGRAM
					#pragma vertex vertexFunction
					#pragma fragment fragmentFunction

					//user defined variables
					uniform float4 _Color;
					uniform float4 _SpecColour;
					uniform float _Shininess;

					//unity defined variables
					uniform float4 _LightColor0;

					//input struct
					struct inputStruct
					{
						float4 vertexPos : POSITION;
						float3 vertexNormal : NORMAL;
					};

					//output struct
					struct outputStruct
					{
						float4 pixelPos : SV_POSITION;
						float4 pixelCol : COLOR;

						float3 normalDirection : TEXCOORD0;
						float4 pixelWorldPos : TEXCOORD1;
					};

					//vertex program
					outputStruct vertexFunction (inputStruct input)
					{
						outputStruct toReturn;
						toReturn.normalDirection = normalize(mul(float4(input.vertexNormal, 0.0), unity_WorldToObject).xyz);
						toReturn.pixelWorldPos = float4(mul(unity_ObjectToWorld, input.vertexPos));
						toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);
						return toReturn;
					}

					//fragment program
					float4 fragmentFunction(outputStruct input) : COLOR
					{
						float3 viewDirection = normalize(float3(float4(_WorldSpaceCameraPos.xyz, 1.0) - mul(unity_ObjectToWorld, input.pixelWorldPos).xyz));
						float3 lightDirection = normalize(-_WorldSpaceLightPos0.xyz);
						float attenuation = 1.0;
						float3 diffuseReflection = attenuation * _LightColor0.xyz * max(0.0, dot(input.normalDirection, lightDirection));
						float3 specularReflection = max(0.0, dot(input.normalDirection, lightDirection)) * pow(max(0.0, dot(reflect(-lightDirection, input.normalDirection), viewDirection)), _Shininess) * attenuation * _SpecColour.rgb;
						float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;
						return float4(finalLight * _Color, 1.0);
					}

					ENDCG
				}
			}

	//Fallback
	//FallBack "Diffuse"		
}
